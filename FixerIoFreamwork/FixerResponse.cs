﻿using System.Net;

namespace FixerIoFreamwork
{
    internal class FixerResponse : IFixerResponse
    {
        public Currency Currency { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}
