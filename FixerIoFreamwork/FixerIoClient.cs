﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixerIoFreamwork
{
    public class FixerIoClient
    {
        public FixerIoClient(string accessKey)
        {
            _accessKey = accessKey;
            _client = new RestClient("http://data.fixer.io/api");
        }
        private readonly string _accessKey;
        private readonly RestClient _client;

        public IFixerResponse GetByDate(DateTime date)
        {
            return OnGet(date.ToString("yyyy-MM-dd"), null);
        }

        public IFixerResponse GetLatest() => OnGet("latest", null);

        //symbols=USD,AUD,CAD,PLN,MXN
        public IFixerResponse GetLatest(params string[] symbols)
        {
            var p = new Parameter("symbols", string.Join(",", symbols), ParameterType.GetOrPost);
            return OnGet("latest", p);
        }


        public IFixerResponse GetLatest(params _Symbols[] symbols)
        {
            string[] srtSymbols = symbols.Select(p => p.ToString()).ToArray();
            return GetLatest(srtSymbols);
        }


        private IFixerResponse OnGet(string methodName, params Parameter[] parameters)
        {
            var request = new RestRequest(methodName, Method.GET);
            request.AddParameter("access_key", _accessKey);

            if (parameters != null && parameters.Length > 0)
            {
                foreach (var p in parameters)
                {
                    request.AddParameter(p);
                }
            }

            IRestResponse<Currency> response = _client.Execute<Currency>(request);
            return new FixerResponse
            {
                StatusCode = response.StatusCode,
                Currency = response.Data
            };
        }
    }
}
