﻿using System.Net;

namespace FixerIoFreamwork
{
    public interface IFixerResponse
    {
        Currency Currency { get; }
        HttpStatusCode StatusCode { get; }
    }
}
